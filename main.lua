require "scripts.rstore_api"
require "scripts.game"
require "scripts.menus"

function love.load()
  love.graphics.setDefaultFilter("nearest","nearest",1)
	g_gamemode = "menus"
	g_loading = true
	g_control = false
  g_keypressed = nil
	g_highscores = nil
	g_mouseclick = nil
	g_font = {}
	g_loadscreen = love.graphics.newImage("art/loading.png")
  g_font = love.graphics.newImageFont("font/LazenbyComp.png","abcdefghijklmnopqrstuvwxyz0123456789:.!?")
  love.graphics.setFont(g_font)
	if love.filesystem.exists("settings.txt") then
		g_settings = rstore_read("settings.txt")
	else
		g_settings = {
			sound=1,
			scale = 1,
      keys={
        left = "left",
        right = "right",
        down = "down",
        up = "up",
        rright = "x",
        rleft = "z",
        accept="space"
      }
		}
		rstore_write("settings.txt",g_settings)
  end
  if love.filesystem.exists("highscores.txt") then
		g_highscores = rstore_read("highscores.txt")
	else
		g_highscores = {
			{
      name = "???",
      score = 3000
      },
      {
      name = "???",
      score = 2000
      },
      {
      name = "???",
      score = 1000
      }
		}
		rstore_write("highscores.txt",g_highscores)
	end
	love.window.setMode(160*g_settings.scale,144*g_settings.scale)
end

function love.update(dt)
	_G[g_gamemode].update(dt)
end

function love.draw()
	if g_loading then
		loading.draw()
	else
		_G[g_gamemode].draw()
	end
end

function love.keypressed(key)
  if g_control then
    g_keypressed=key
    print (g_keypressed)
  end
end

loading={}
function loading.draw()
  love.graphics.draw(g_loadscreen,0,0,0,scale,scale)
end