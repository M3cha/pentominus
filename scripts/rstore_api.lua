local function rstore_expand(a_string,a_table)
  local l_first = true
  for i,v in pairs(a_table) do
    if l_first then
      l_first = false
      if type(i) == "string" then
        a_string = a_string..i.."="
      end
    else
      if type(i) == "string" then
        a_string = a_string..","..i.."="
      else
        a_string = a_string..","
      end
    end
    if type(v) == "table" then
      a_string = a_string.."{"
      a_string = rstore_expand(a_string,v)
    elseif type(v) == "string" then
      a_string = a_string.."\""..v.."\""
    else
      a_string = a_string..v
    end
  end
  a_string = a_string.."}"
  return a_string
end

function rstore_write(a_filename,a_table)
  local l_string = "return "
  if type(a_table) == "table" then
    l_string = l_string.."{"
    l_string = rstore_expand(l_string,a_table)
  elseif type(a_table) == "string" then
    l_string = l_string.."\""..a_table.."\""
  else
    l_string = l_string..a_table
  end
  print(love.filesystem.write(a_filename,l_string))
end

function rstore_read(a_filename)
  return loadstring(love.filesystem.read(a_filename))()
end