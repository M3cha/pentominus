menus={splash={},title={},settings={},highscores={}}

function menus.load()
  g_menus={
    menu="splash",
    pos=1,
    controlchanging=false,
    tim3=1,
    textures={
    title=love.graphics.newImage("art/title.png"),
    arrow=love.graphics.newImage("art/arrow.png"),
    splash=love.graphics.newImage("art/splash.png"),
    bg=love.graphics.newImage("art/bg.png")
    }
  }
  if g_launching==false then
    g_menus.menu="title"
    g_menus.tim3=.5
  end
  g_loading=false
end

function menus.update(dt)
  if g_loading then
    menus.load()
  end
  g_menus.tim3=g_menus.tim3-dt
  menus[g_menus.menu].update()
end

function menus.draw()
  menus[g_menus.menu].draw()
end

function menus.splash.update()
  if g_menus.tim3 <= 0 then
    g_menus.menu = "title"
    g_control = true
    g_menus.tim3 = .5
    g_launching = false
  end
end

function menus.splash.draw()
  love.graphics.draw(g_menus.textures.splash,0,0,0,g_settings.scale,g_settings.scale)
end

function menus.title.update()
  if g_menus.tim3 <= 0 then
    g_menus.tim3 = .5
  end
  if g_keypressed then
    if g_keypressed == g_settings.keys.up then
      if g_menus.pos > 1 then
        g_menus.pos = g_menus.pos - 1
      end
    elseif g_keypressed == g_settings.keys.down then
      if g_menus.pos < 3 then
        g_menus.pos = g_menus.pos + 1
      end
    elseif g_keypressed == g_settings.keys.accept then
      if g_menus.pos == 1 then
        g_gamemode = "game"
        g_loading = true
        g_menus = nil
      elseif g_menus.pos == 2 then
        g_menus.menu = "highscores"
        g_menus.tim3 = .5
        g_menus.pos = 1
      elseif g_menus.pos == 3 then
        g_menus.menu = "settings"
        g_menus.tim3 = .5
        g_menus.pos = 1
      end
    end
    g_keypressed = nil
  end
end

function menus.title.draw()
  love.graphics.draw(g_menus.textures.title,0,0,0,g_settings.scale,g_settings.scale)
  if g_menus.tim3 >= .1 then
    if g_menus.pos == 1 then
      love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,64*g_settings.scale,0,g_settings.scale,g_settings.scale)
    elseif g_menus.pos == 2 then
      love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,80*g_settings.scale,0,g_settings.scale,g_settings.scale)
    elseif g_menus.pos == 3 then
      love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,96*g_settings.scale,0,g_settings.scale,g_settings.scale)
    end
  end
end

function menus.highscores.update()
  if g_menus.tim3 <= 0 then
    g_menus.tim3 = .5
  end
  if g_keypressed then
    if g_keypressed == g_settings.keys.accept then
      g_menus.menu = "title"
      g_menus.tim3 = .5
      g_menus.pos = 1
    end
    g_keypressed = nil
  end
end

function menus.highscores.draw()
  love.graphics.draw(g_menus.textures.bg,0,0,0,g_settings.scale,g_settings.scale)
  love.graphics.print("high scores", 32*g_settings.scale, 32*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.print(g_highscores[1].name,24*g_settings.scale,64*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.printf(g_highscores[1].score,64*g_settings.scale,64*g_settings.scale,73,"right",0,g_settings.scale,g_settings.scale)
  love.graphics.print(g_highscores[2].name,24*g_settings.scale,80*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.printf(g_highscores[2].score,64*g_settings.scale,80*g_settings.scale,73,"right",0,g_settings.scale,g_settings.scale)
  love.graphics.print(g_highscores[3].name,24*g_settings.scale,96*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.printf(g_highscores[3].score,64*g_settings.scale,96*g_settings.scale,73,"right",0,g_settings.scale,g_settings.scale)
end

function menus.settings.update()
  if g_menus.tim3 <= 0 then
    g_menus.tim3 = .5
  end
  if g_keypressed then
    if g_keypressed == g_settings.keys.up then
      if g_menus.pos > 1 then
        g_menus.pos = g_menus.pos - 1
      end
    elseif g_keypressed == g_settings.keys.down then
      if g_menus.pos < 3 then
        g_menus.pos = g_menus.pos + 1
      end
    elseif g_keypressed == g_settings.keys.accept then
      if g_menus.pos == 1 then
        if g_settings.sound == 0 then
          g_settings.sound = 1
        else
          g_settings.sound = 0
        end
      elseif g_menus.pos == 2 then
        if g_settings.scale == 1 then
          g_settings.scale = 2
          love.window.setMode(160*g_settings.scale,144*g_settings.scale)
        elseif g_settings.scale == 2 then
          g_settings.scale = 4
          love.window.setMode(160*g_settings.scale,144*g_settings.scale)
        elseif g_settings.scale == 4 then
          g_settings.scale = 8
          love.window.setMode(160*g_settings.scale,144*g_settings.scale)
        elseif g_settings.scale == 8 then
          g_settings.scale = 1
          love.window.setMode(160*g_settings.scale,144*g_settings.scale)
        end
      --elseif g_menus.pos == 3 then
        --g_menus.menu = "controls"
      elseif g_menus.pos == 3 then
        g_menus.menu = "title"
        rstore_write("settings.txt",g_settings)
        g_menus.tim3 = .5
        g_menus.pos = 1
      end
    end
    g_keypressed = nil
  end
end

function menus.settings.draw()
  love.graphics.draw(g_menus.textures.bg,0,0,0,g_settings.scale,g_settings.scale)
  love.graphics.print("settings", 32*g_settings.scale, 32*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.print("sound",24*g_settings.scale,56*g_settings.scale,0,g_settings.scale,g_settings.scale)
  if g_settings.sound == 0 then
    love.graphics.printf("off",64*g_settings.scale,56*g_settings.scale,73,"right",0,g_settings.scale,g_settings.scale)
  else
    love.graphics.printf("on",64*g_settings.scale,56*g_settings.scale,73,"right",0,g_settings.scale,g_settings.scale)
  end
  love.graphics.print("scale",24*g_settings.scale,72*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.printf(g_settings.scale,64*g_settings.scale,72*g_settings.scale,73,"right",0,g_settings.scale,g_settings.scale)
  --love.graphics.print("controls",24*g_settings.scale,88*g_settings.scale,0,g_settings.scale,g_settings.scale)
  love.graphics.print("return",24*g_settings.scale,104*g_settings.scale,0,g_settings.scale,g_settings.scale)
  if g_menus.tim3 >= .1 then
    if g_menus.pos == 1 then
      love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,56*g_settings.scale,0,g_settings.scale,g_settings.scale)
    elseif g_menus.pos == 2 then
      love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,72*g_settings.scale,0,g_settings.scale,g_settings.scale)
    --elseif g_menus.pos == 3 then
      --love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,88*g_settings.scale,0,g_settings.scale,g_settings.scale)
    elseif g_menus.pos == 3 then
      love.graphics.draw(g_menus.textures.arrow,8*g_settings.scale,104*g_settings.scale,0,g_settings.scale,g_settings.scale)
    end
  end
end