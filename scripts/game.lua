game={}

function game.load()
  g_game = {
    score = 0,
    linescleared = 0,
    clearinglines = false,
    map = {},
    currentblock = {},
    currentpos={},
    nextblock={},
    tim3 = 1,
    speed = .4,
    paused = false,
    gameover = false,
    textures = {
      love.graphics.newImage("art/1.png"),
      love.graphics.newImage("art/2.png"),
      love.graphics.newImage("art/3.png"),
      love.graphics.newImage("art/4.png"),
      love.graphics.newImage("art/5.png"),
      love.graphics.newImage("art/6.png"),
      love.graphics.newImage("art/7.png"),
      love.graphics.newImage("art/8.png"),
      love.graphics.newImage("art/9.png"),
      love.graphics.newImage("art/10.png"),
      love.graphics.newImage("art/11.png"),
      love.graphics.newImage("art/12.png"),
      love.graphics.newImage("art/13.png"),
      love.graphics.newImage("art/14.png"),
      love.graphics.newImage("art/15.png"),
      love.graphics.newImage("art/16.png"),
      love.graphics.newImage("art/17.png"),
      love.graphics.newImage("art/18.png"),
      love.graphics.newImage("art/19.png"),
      love.graphics.newImage("art/clearrow.png"),
      love.graphics.newImage("art/bg.png")
    }
  }
  for x=1, 12 do
    g_game.map[x]={}
  	for y=1, 18 do
      g_game.map[x][y]=0
    end
  end
  game.generate_block()
  g_game.currentblock = g_game.nextblock
  game.generate_block()
  g_game.currentpos={x=3,y=-2}
  g_loading = false
  g_launching = true
end

function game.update(dt)
  if g_loading then
    game.load()
    g_control = true
    return
  end
  if g_game.paused then
    if g_keypressed then
      if g_keypressed == g_settings.keys.accept then
        g_game.paused = false
      end
      g_keypressed = nil
    end
    return
  end
  if g_game.gameover then
    if g_keypressed then
      if g_keypressed == g_settings.keys.accept then
        if g_game.score <= g_highscores[3].score then
          g_gamemode = "menus"
          g_loading = true
          g_game = nil
        else
          g_gamemode = "nameentry"
          g_loading = true
        end
      end
      g_keypressed = nil
    end
    return
  end
  g_game.tim3=g_game.tim3-dt
  if g_game.clearinglines and g_game.tim3 <= 0 then --Tests if lines have just been cleared.
    game.clearlines()
    g_game.tim3 = g_game.speed
    g_game.currentblock=g_game.nextblock
    g_game.currentpos={x=3,y=-2}
    game.generate_block()
    g_control = true
  elseif g_game.clearinglines then
    return
  end
  if g_keypressed then
    if g_keypressed == g_settings.keys.accept then
      g_game.paused = true
      print "pausing"
    elseif g_keypressed == g_settings.keys.left then
      if game.check_block(g_game.currentblock,{x=g_game.currentpos.x-1,y=g_game.currentpos.y}) then
        g_game.currentpos.x=g_game.currentpos.x-1
      end
    elseif g_keypressed == g_settings.keys.right then
      if game.check_block(g_game.currentblock,{x=g_game.currentpos.x+1,y=g_game.currentpos.y}) then
        g_game.currentpos.x=g_game.currentpos.x+1
      end
    elseif g_keypressed == g_settings.keys.rleft then
      if game.check_block(game.rotate_block(g_game.currentblock,"left"),g_game.currentpos) then
        g_game.currentblock=game.rotate_block(g_game.currentblock,"left")
      end
    elseif g_keypressed == g_settings.keys.rright then
      if game.check_block(game.rotate_block(g_game.currentblock,"right"),g_game.currentpos) then
        g_game.currentblock=game.rotate_block(g_game.currentblock,"right")
      end
    elseif g_keypressed == g_settings.keys.down then
      g_game.tim3 = 0
    end
    g_keypressed=nil
  end
  if g_game.tim3 <= 0 then --if tim3 <= 0 then move or cement the currentblock
    if game.check_block(g_game.currentblock,{x=g_game.currentpos.x,y=g_game.currentpos.y+1}) then
      g_game.currentpos.y=g_game.currentpos.y+1
    else
      if game.cement() then
        g_game.clearinglines = game.check_map()
        if g_game.clearinglines then --if there are lines to be cleared then spend half a second clearing them
          g_game.tim3=.5
          g_control = false
          g_game.currentblock=nil
        else --If there aren't, then load the next block
          g_game.currentblock=g_game.nextblock
          g_game.currentpos={x=3,y=-2}
          game.generate_block()
        end
      else
        g_game.gameover=true
      end
    end
    g_game.tim3 = g_game.speed
  end
end

function game.draw()
  if g_loading then
    love.graphics.draw(g_loadscreen,0,0,0,scale,scale)
  elseif g_game.gameover then
    --draw game over screen
    love.graphics.draw(g_game.textures[21],0,0,0,g_settings.scale,g_settings.scale)
    love.graphics.print("game over", 32*g_settings.scale, 32*g_settings.scale,0,g_settings.scale,g_settings.scale)
  elseif g_game.paused then
    --draw paused screen
    love.graphics.draw(g_game.textures[21],0,0,0,g_settings.scale,g_settings.scale)
    love.graphics.print("paused", 32*g_settings.scale, 32*g_settings.scale,0,g_settings.scale,g_settings.scale)
  else
    love.graphics.draw(g_game.textures[19],0,0,0,g_settings.scale,g_settings.scale)
    for x=1,12 do
      for y=1,18 do
        if g_game.map[x][y] ~= 0 then
          love.graphics.draw(g_game.textures[g_game.map[x][y]], x*8*g_settings.scale, (y-1)*8*g_settings.scale,0,g_settings.scale,g_settings.scale)
        end
      end
    end
    if g_game.clearinglines then
      if g_game.tim3 > 0 and g_game.tim3 <= .1 then
        for i,v in ipairs(g_game.clearinglines) do
          love.graphics.draw(g_game.textures[20],8*g_settings.scale,(v-1)*8*g_settings.scale,0,g_settings.scale,g_settings.scale)
        end
      elseif g_game.tim3 > .2 and g_game.tim3 <= .3 then
        for i,v in ipairs(g_game.clearinglines) do
          love.graphics.draw(g_game.textures[20],8*g_settings.scale,(v-1)*8*g_settings.scale,0,g_settings.scale,g_settings.scale)
        end
      elseif g_game.tim3 > .4 and g_game.tim3 <= .5 then
        for i,v in ipairs(g_game.clearinglines) do
          love.graphics.draw(g_game.textures[20],8*g_settings.scale,(v-1)*8*g_settings.scale,0,g_settings.scale,g_settings.scale)
        end
      end
    else
      for x=1,5 do
        for y=1,5 do
          if g_game.currentblock[x][y] ~= 0 then
            love.graphics.draw(g_game.textures[g_game.currentblock[x][y] ], (g_game.currentpos.x+x-1)*8*g_settings.scale, (g_game.currentpos.y+y-2)*8*g_settings.scale,0,g_settings.scale,g_settings.scale)
          end
        end
      end
    end
    for x=1,5 do
      for y=1,5 do
        if g_game.nextblock[x][y] ~= 0 then
          love.graphics.draw(g_game.textures[g_game.nextblock[x][y] ], (x+13)*8*g_settings.scale, (y+10)*8*g_settings.scale,0,g_settings.scale,g_settings.scale)
        end
      end
    end
    love.graphics.print("score",112*g_settings.scale,8*g_settings.scale,0,g_settings.scale,g_settings.scale)
    love.graphics.printf(g_game.score,112*g_settings.scale,16*g_settings.scale,41,"right",0,g_settings.scale,g_settings.scale)
    love.graphics.print("lines",112*g_settings.scale,40*g_settings.scale,0,g_settings.scale,g_settings.scale)
    love.graphics.printf(g_game.linescleared,112*g_settings.scale,48*g_settings.scale,41,"right",0,g_settings.scale,g_settings.scale)
  end
end

function game.generate_block()
  local newblock=0
  if g_game.linescleared == 0 then
    newblock=love.math.random(4)
  elseif g_game.linescleared >= 1 and g_game.linescleared < 5 then
    newblock=love.math.random(16)
  else
    newblock=love.math.random(18)
  end
  if newblock==1 then
    g_game.nextblock={{0,0,0,0,0},{0,0,1,1,0},{0,0,1,1,0},{0,0,0,1,0},{0,0,0,0,0}}
  elseif newblock==2 then
    g_game.nextblock={{0,0,0,0,0},{0,0,0,2,0},{0,0,2,2,0},{0,0,2,2,0},{0,0,0,0,0}}
  elseif newblock==3 then
    g_game.nextblock={{0,0,0,0,0},{0,0,0,3,0},{0,0,0,3,0},{0,3,3,3,0},{0,0,0,0,0}}
  elseif newblock==4 then
    g_game.nextblock={{0,0,0,0,0},{0,0,4,0,0},{0,0,4,0,0},{0,4,4,4,0},{0,0,0,0,0}}
  elseif newblock==5 then
    g_game.nextblock={{0,0,0,0,0},{0,0,0,5,0},{0,5,5,5,0},{0,5,0,0,0},{0,0,0,0,0}}
  elseif newblock==6 then
    g_game.nextblock={{0,0,0,0,0},{0,6,0,0,0},{0,6,6,6,0},{0,0,0,6,0},{0,0,0,0,0}}
  elseif newblock==7 then
    g_game.nextblock={{0,0,0,0,0},{0,0,0,7,0},{0,0,7,7,0},{0,7,7,0,0},{0,0,0,0,0}}
  elseif newblock==8 then
    g_game.nextblock={{0,0,8,0,0},{0,0,8,0,0},{0,0,8,0,0},{0,0,8,0,0},{0,0,8,0,0}}
  elseif newblock==9 then
    g_game.nextblock={{0,0,0,0,0},{0,9,9,0,0},{0,0,9,0,0},{0,9,9,0,0},{0,0,0,0,0}}
  elseif newblock==10 then
    g_game.nextblock={{0,0,0,0,0},{0,0,10,0,0},{0,10,10,10,0},{0,0,10,0,0},{0,0,0,0,0}}
  elseif newblock==11 then
    g_game.nextblock={{0,0,0,0,0},{0,0,11,11,0},{0,0,0,11,0},{0,0,0,11,0},{0,0,0,11,0}}
  elseif newblock==12 then
    g_game.nextblock={{0,0,0,12,0},{0,0,0,12,0},{0,0,0,12,0},{0,0,12,12,0},{0,0,0,0,0}}
  elseif newblock==13 then
    g_game.nextblock={{0,0,0,0,0},{0,0,0,13,0},{0,0,0,13,0},{0,0,13,13,0},{0,0,0,13,0}}
  elseif newblock==14 then
    g_game.nextblock={{0,0,0,14,0},{0,0,14,14,0},{0,0,0,14,0},{0,0,0,14,0},{0,0,0,0,0}}
  elseif newblock==15 then
    g_game.nextblock={{0,0,0,0,0},{0,0,0,15,0},{0,0,0,15,0},{0,0,15,15,0},{0,0,15,0,0}}
  elseif newblock==16 then
    g_game.nextblock={{0,0,16,0,0},{0,0,16,16,0},{0,0,0,16,0},{0,0,0,16,0},{0,0,0,0,0}}
  elseif newblock==17 then
    g_game.nextblock={{0,0,0,0,0},{0,0,17,0,0},{0,17,17,0,0},{0,0,17,17,0},{0,0,0,0,0}}
  elseif newblock==18 then
    g_game.nextblock={{0,0,0,0,0},{0,0,18,18,0},{0,18,18,0,0},{0,0,18,0,0},{0,0,0,0,0}}
  else
    print "Random failed to load an actual number"
  end
  print ("generated block: "..newblock)
end

-- 1 is clockwise, 2 is counter-clockwise
function game.rotate_block (a_block, a_dir)
  local newblock={{},{},{},{},{}}
  if a_dir=="right" then
    newblock[1][1]=a_block[1][5]
    newblock[1][2]=a_block[2][5]
    newblock[1][3]=a_block[3][5]
    newblock[1][4]=a_block[4][5]
    newblock[1][5]=a_block[5][5]
    
    newblock[2][1]=a_block[1][4]
    newblock[2][2]=a_block[2][4]
    newblock[2][3]=a_block[3][4]
    newblock[2][4]=a_block[4][4]
    newblock[2][5]=a_block[5][4]
    
    newblock[3][1]=a_block[1][3]
    newblock[3][2]=a_block[2][3]
    newblock[3][3]=a_block[3][3]
    newblock[3][4]=a_block[4][3]
    newblock[3][5]=a_block[5][3]
    
    newblock[4][1]=a_block[1][2]
    newblock[4][2]=a_block[2][2]
    newblock[4][3]=a_block[3][2]
    newblock[4][4]=a_block[4][2]
    newblock[4][5]=a_block[5][2]
    
    newblock[5][1]=a_block[1][1]
    newblock[5][2]=a_block[2][1]
    newblock[5][3]=a_block[3][1]
    newblock[5][4]=a_block[4][1]
    newblock[5][5]=a_block[5][1]
  elseif a_dir=="left" then
    newblock[1][1]=a_block[5][1]
    newblock[1][2]=a_block[4][1]
    newblock[1][3]=a_block[3][1]
    newblock[1][4]=a_block[2][1]
    newblock[1][5]=a_block[1][1]
    
    newblock[2][1]=a_block[5][2]
    newblock[2][2]=a_block[4][2]
    newblock[2][3]=a_block[3][2]
    newblock[2][4]=a_block[2][2]
    newblock[2][5]=a_block[1][2]
    
    newblock[3][1]=a_block[5][3]
    newblock[3][2]=a_block[4][3]
    newblock[3][3]=a_block[3][3]
    newblock[3][4]=a_block[2][3]
    newblock[3][5]=a_block[1][3]
    
    newblock[4][1]=a_block[5][4]
    newblock[4][2]=a_block[4][4]
    newblock[4][3]=a_block[3][4]
    newblock[4][4]=a_block[2][4]
    newblock[4][5]=a_block[1][4]
    
    newblock[5][1]=a_block[5][5]
    newblock[5][2]=a_block[4][5]
    newblock[5][3]=a_block[3][5]
    newblock[5][4]=a_block[2][5]
    newblock[5][5]=a_block[1][5]
  end
  return newblock
end

function game.check_block(a_block, a_pos)
  for x=1, 5 do
    for y=1, 5 do
      if a_block[x][y] ~= 0 then
        if a_pos.x+x-1<1 or a_pos.x+x-1>12 then
          print ("check_block returned false -- Off Edge of Map")
          return false
        elseif a_pos.y+y-1>18 then
          print("check_block returned false -- Off Bottom of Map")
          return false
        elseif a_pos.y+y-1>1 and g_game.map[a_pos.x+x-1][a_pos.y+y-1] ~= 0 then
          print ("check_block returned false -- Blocked By other Block")
          return false
        end
      end
    end
  end
  return true
end

function game.cement()
  for x=1, 5 do
    for y=1, 5 do
      if g_game.currentblock[x][y] ~= 0 then
        if g_game.currentpos.y+y-1<0 then
          return false
        else
          g_game.map[g_game.currentpos.x+x-1][g_game.currentpos.y+y-1]=g_game.currentblock[x][y]
        end
      end
    end
  end
  return true
end

function game.check_map()
  local l_cleared={}
  for y=1, 18 do
    local l_row=true
    for x=1, 12 do
      if g_game.map[x][y]==0 then
        l_row=false
      end
    end
    if l_row==true then
      table.insert(l_cleared,y)
    end
  end
  if l_cleared[1] then
    return l_cleared
  else
    return false
  end
end

function game.clearlines()
  g_game.score = g_game.score+#g_game.clearinglines*#g_game.clearinglines*10
  for i, v in ipairs(g_game.clearinglines) do
    g_game.linescleared = g_game.linescleared + 1
    print ("clearing line: "..v)
    for x=1, 12 do
      g_game.map[x][v]=0
    end
    for y=v, 2, -1 do
      for x=1,12 do
        g_game.map[x][y]=g_game.map[x][y-1]
        g_game.map[x][y-1] = 0
      end
    end
  end
  g_game.speed=.4/math.ceil(g_game.linescleared*.25)
  if g_game.speed < .1 then
    g_game.speed = .1
  end
  g_game.clearinglines=false
end

nameentry={}

function nameentry.update()
  if g_loading then
    g_loading = false
    g_nameentry = {name={"_","_","_"},place=nil, namestr = "", pos = 1}
    if g_game.score > g_highscores[1].score then
      g_nameentry.place = 1
      g_highscores[3] = g_highscores[2]
      g_highscores[2] = g_highscores[1]
    elseif g_game.score > g_highscores[2].score then
      g_nameentry.place = 2
      g_highscores[3] = g_highscores[2]
    else
      g_nameentry.place = 3
    end
  end
  if g_keypressed then
    if g_keypressed == "space" then
      if g_nameentry.name[1] ~= "_" then
        for i=1,3 do
          if g_nameentry.name[i] ~= "_" then
            g_nameentry.namestr = g_nameentry.namestr..g_nameentry.name[i]
          end
        end
      end
      g_highscores[g_nameentry.place] = {name = g_nameentry.namestr, score = g_game.score}
      rstore_write("highscores.txt",g_highscores)
      g_gamemode = "menus"
      g_loading = true
      g_game = nil
      g_nameentry = nil
    elseif g_keypressed:match("%W") ~= false and g_keypressed:len() == 1 then
      g_nameentry.name[g_nameentry.pos] = g_keypressed:lower()
      if g_nameentry.pos < 3 then
        g_nameentry.pos = g_nameentry.pos + 1
      end
    end
    g_keypressed = nil
  end
end

function nameentry.draw()
  love.graphics.draw(g_game.textures[21],0,0,0,g_settings.scale,g_settings.scale)
  love.graphics.print("congratulations!", 32*g_settings.scale, 32*g_settings.scale,0,g_settings.scale,g_settings.scale)
  if g_nameentry.place == 1 then
    love.graphics.print("first place", 24*g_settings.scale, 64*g_settings.scale,0,g_settings.scale,g_settings.scale)
  elseif g_nameentry.place == 1 then
    love.graphics.print("second place", 24*g_settings.scale, 64*g_settings.scale,0,g_settings.scale,g_settings.scale)
  else
    love.graphics.print("third place", 24*g_settings.scale, 64*g_settings.scale,0,g_settings.scale,g_settings.scale)
  end
  love.graphics.print("enter your name: "..g_nameentry.name[1]..g_nameentry.name[2]..g_nameentry.name[3], 24*g_settings.scale, 80*g_settings.scale,0,g_settings.scale,g_settings.scale)
end